\select@language {english}
\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Description of the Company}{2}
\contentsline {subsection}{\numberline {2.1}Company Name}{3}
\contentsline {subsection}{\numberline {2.2}Company Location}{3}
\contentsline {subsection}{\numberline {2.3}General Description of the Company}{3}
\contentsline {subsection}{\numberline {2.4}A Brief History of the Company}{5}
\contentsline {section}{\numberline {3}Orientation \& Useful Programs}{6}
\contentsline {subsection}{\numberline {3.1}Pomodoro Technique \& Pomotodo App}{7}
\contentsline {subsection}{\numberline {3.2}Database Structure \& Airtable}{8}
\contentsline {subsection}{\numberline {3.3}Wiki Pages \& Confluence Wiki }{9}
\contentsline {subsection}{\numberline {3.4}V-Model \& Agile Methodology}{10}
\contentsline {subsection}{\numberline {3.5}Version Control with Git \& Bitbucket}{11}
\contentsline {subsection}{\numberline {3.6}Microsoft Sharepoint}{12}
\contentsline {section}{\numberline {4}Python \& Raspberry Pi}{12}
\contentsline {section}{\numberline {5}Solar Tracker System Project }{15}
\contentsline {section}{\numberline {6}MATLAB}{15}
\contentsline {section}{\numberline {7}Conclusion}{15}
\contentsline {section}{\numberline {8}References }{16}
