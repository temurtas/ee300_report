\select@language {english}
\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Description of the Company}{3}
\contentsline {subsection}{\numberline {2.1}Company Name}{3}
\contentsline {subsection}{\numberline {2.2}Company Location}{4}
\contentsline {subsection}{\numberline {2.3}General Description of the Company}{4}
\contentsline {subsection}{\numberline {2.4}The Organizational Chart of the Company}{5}
\contentsline {subsection}{\numberline {2.5}A Brief History of the Company}{6}
\contentsline {section}{\numberline {3}Orientation \& Useful Programs}{7}
\contentsline {subsection}{\numberline {3.1}Pomodoro Technique}{7}
\contentsline {subsubsection}{\numberline {3.1.1}Pomotodo App}{8}
\contentsline {subsection}{\numberline {3.2}Database Structure}{10}
\contentsline {subsubsection}{\numberline {3.2.1}Airtable}{10}
\contentsline {subsection}{\numberline {3.3}Wiki Pages}{11}
\contentsline {subsubsection}{\numberline {3.3.1}Confluence Wiki}{12}
\contentsline {subsection}{\numberline {3.4}V-Model \& Agile Methodology}{13}
\contentsline {subsubsection}{\numberline {3.4.1}V-Model}{13}
\contentsline {subsubsection}{\numberline {3.4.2}Agile Methodology (Scrum) }{15}
\contentsline {paragraph}{\numberline {3.4.2.1}Roles}{15}
\contentsline {subsection}{\numberline {3.5}Version Control with Git}{15}
\contentsline {subsubsection}{\numberline {3.5.1}Github}{16}
\contentsline {subsubsection}{\numberline {3.5.2}Bitbucket}{16}
\contentsline {section}{\numberline {4}Solar Tracker System Project }{17}
\contentsline {subsection}{\numberline {4.1}Planning \& Researching}{17}
\contentsline {subsubsection}{\numberline {4.1.1}System Requirements}{19}
\contentsline {subsubsection}{\numberline {4.1.2}Subsystem Requirements}{19}
\contentsline {subsubsection}{\numberline {4.1.3}Component Requirements}{20}
\contentsline {subsubsection}{\numberline {4.1.4}Components}{20}
\contentsline {subsection}{\numberline {4.2}Training}{21}
\contentsline {subsubsection}{\numberline {4.2.1}Training on Python}{21}
\contentsline {paragraph}{\numberline {4.2.1.1}Basics }{21}
\contentsline {paragraph}{\numberline {4.2.1.2}Using Conditions}{23}
\contentsline {paragraph}{\numberline {4.2.1.3}Using Loops}{23}
\contentsline {paragraph}{\numberline {4.2.1.4}Defining Functions}{23}
\contentsline {paragraph}{\numberline {4.2.1.5}Defining Classes}{24}
\contentsline {subsection}{\numberline {4.3}Working on the Project}{25}
\contentsline {subsubsection}{\numberline {4.3.1}Working on Raspberry Pi}{25}
\contentsline {paragraph}{\numberline {4.3.1.1}Training on LEDs}{25}
\contentsline {paragraph}{\numberline {4.3.1.2}Training on LDRs}{26}
\contentsline {paragraph}{\numberline {4.3.1.3}Training on Servo Motors}{26}
\contentsline {subsubsection}{\numberline {4.3.2}Raspberry Pi Final Code}{27}
\contentsline {subsubsection}{\numberline {4.3.3}Working on Arduino}{28}
\contentsline {paragraph}{\numberline {4.3.3.1}Training on LEDs \& Pins}{28}
\contentsline {paragraph}{\numberline {4.3.3.2}Training on Servo Motors}{28}
\contentsline {subsubsection}{\numberline {4.3.4}Final Arduino Code}{29}
\contentsline {subsection}{\numberline {4.4}Implementation}{31}
\contentsline {subsubsection}{\numberline {4.4.1}PCB Drawing}{31}
\contentsline {subsubsection}{\numberline {4.4.2}3D Drawings}{32}
\contentsline {subsubsection}{\numberline {4.4.3}Construction of the Body}{32}
\contentsline {paragraph}{\numberline {4.4.3.1}Top Layer}{32}
\contentsline {paragraph}{\numberline {4.4.3.2}Main Body}{33}
\contentsline {paragraph}{\numberline {4.4.3.3}Solar Panel}{33}
\contentsline {paragraph}{\numberline {4.4.3.4}Final Body}{34}
\contentsline {subsection}{\numberline {4.5}Tests}{34}
\contentsline {subsection}{\numberline {4.6}Project Tracking}{35}
\contentsline {subsubsection}{\numberline {4.6.1}Kanban}{35}
\contentsline {section}{\numberline {5}After Project}{36}
\contentsline {subsection}{\numberline {5.1}Training on MATLAB}{36}
\contentsline {subsubsection}{\numberline {5.1.1}Coursera}{36}
\contentsline {subsubsection}{\numberline {5.1.2}Outline of the Course}{36}
\contentsline {paragraph}{\numberline {5.1.2.1}Simple Sorting Code}{37}
\contentsline {subsection}{\numberline {5.2}Training on Microsoft Sharepoint}{39}
\contentsline {subsubsection}{\numberline {5.2.1}Microsoft Sharepoint}{39}
\contentsline {section}{\numberline {6}Conclusion}{41}
\contentsline {section}{\numberline {7}References }{42}
